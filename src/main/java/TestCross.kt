import com.fasterxml.jackson.databind.ObjectMapper
import java.io.File
import java.util.*
import javax.crypto.Cipher
import javax.crypto.KeyGenerator
import javax.crypto.spec.SecretKeySpec


val keyGen = KeyGenerator.getInstance("AES").also { it.init(256) }
val mapper = ObjectMapper()

/**
 * Functions to Decode Password
 * @param file - File with a map saved in encodePassword
 * @param combination - Combination to use, for example 012
 * @param keys - Keys in order given in combination parameter
 */
fun decode(file: File, combination: String, keys: List<String>): String {
    val map = mapper.readValue<Map<String, String>>(
        file,
        mapper.typeFactory.constructMapType(Map::class.java, String::class.java, String::class.java)
    )
    var toDecode = map[combination] ?: throw RuntimeException()

    keys.reversed().forEachIndexed { i, it ->
        // println(it)
        toDecode = decrypt(Base64.getDecoder().decode(it), toDecode)
    }
    return toDecode
}

/**
 * Encodes password using given number of Keys, returns keys list
 * @param file - File to save combinations map
 * @param number - Number of keys to generate
 * @param password - Password to encode
 */
fun encodePassword(file: File, number: Int, password: String): List<String> {
    var map = mutableMapOf<String, String>()
    val keys = genKeys(number)
    keys.forEachIndexed { index, bytes ->
        println("$index key -> ${Base64.getEncoder().encodeToString(bytes)}")
    }
    val jumps = (number / 2) + 1

    keys.forEachIndexed { i, it ->
        map[i.toString()] = encrypt(it, password)
    }
    (1 until jumps).forEach { _ ->
        val copy = map.toMutableMap()
        map.clear()
        copy.forEach { key, value ->
            keys.forEachIndexed { i, it ->
                val k = key + i
                if (!map.containsKey(k.reversed()))
                    map[k] = encrypt(it, value)

            }
        }
        map = map.filter { it.key.toList() == it.key.toCharArray().distinct() }.toMutableMap()
    }
    mapper.writeValue(file, map)
    return keys.map { Base64.getEncoder().encodeToString(it) }
}


//Main, for tests
fun main() {

}

//Private Functions
/**
 * Generates number of given keys
 */
fun genKeys(numberOfKeys: Int): List<ByteArray> {
    return (0 until numberOfKeys).map { keyGen.generateKey().encoded }
}

/**
 * Encrypts key, using AES algorithm
 */
fun encrypt(key: ByteArray, value: String): String {
    val skeySpec = SecretKeySpec(key, "AES")
    val cipher = Cipher.getInstance("AES")
    cipher.init(Cipher.ENCRYPT_MODE, skeySpec)
    val encrypted = cipher.doFinal(value.toByteArray())
    return String(Base64.getEncoder().encode(encrypted))
}

/**
 * Decrypts key, using AES algorithm
 */
fun decrypt(key: ByteArray, encrypted: String): String {
    val skeySpec = SecretKeySpec(key, "AES")

    val cipher = Cipher.getInstance("AES")
    cipher.init(Cipher.DECRYPT_MODE, skeySpec)
    //  println(encrypted)
    val original = cipher.doFinal(Base64.getDecoder().decode(encrypted))

    return String(original)
}